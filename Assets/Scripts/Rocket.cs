﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    [SerializeField] private float force;
    [SerializeField] private float rotateSpeed;
    private AudioSource audioSource;
    private Rigidbody rocket;


    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        rocket = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Thrust();
        Rotate();
    }

    void Rotate()
    {
        rocket.freezeRotation = true;

        if (Input.GetKey(KeyCode.LeftArrow)) {
            transform.Rotate(Vector3.forward * Time.deltaTime * rotateSpeed);
        }else if (Input.GetKey(KeyCode.RightArrow)) {
            transform.Rotate(-Vector3.forward * Time.deltaTime * rotateSpeed);
        }

        rocket.freezeRotation = false;
    }

    private void Thrust()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            rocket.AddRelativeForce(Vector3.up * Time.deltaTime * force);
            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }
        }
        else if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            audioSource.Stop();
        }
    }
}

